# Application : asciistat cypher cli program
#	Description
# Author Raymond Thomson

all: help

CC=gcc
MAN=man/
SRC=src/
CFLAGS=-I.
LIBS=-lm

%.o: %.c
	@echo 'Invoking: GCC C object files'
	$(CC) -c -o $($SRC)$@ $< $(CFLAGS)

asciistat: $(SRC)asciistat.o
	@echo 'Building and linking target: $@'
	$(CC) -o asciistat $(SRC)*.o $(LIBS)

local: asciistat clean

clean:
	@echo 'Cleaning build objects'
	rm -f $(SRC)*.o 
	@echo 'Installed. Enter ./asciistat to run'

install: asciistat 
	@echo 'Installing'	
	cp asciistat /usr/local/bin/asciistat
	cp $(MAN)asciistat /usr/share/man/man1/asciistat.1
	gzip /usr/share/man/man1/asciistat.1
	@echo 'Cleaning build objects'
	rm -f $(SRC)*.o 
	rm -f asciistat
	@echo 'installed, type asciistat to run or man asciistat for the manual'

remove:
	rm -f asciistat

uninstall:
	rm -f /usr/local/bin/asciistat
	rm -f /usr/share/man/man1/asciistat.1.gz
	@echo 'asciistat uninstalled.'

help:
	@echo 'Make options for asciistat'
	@echo 'Local Folder make'
	@echo '    make local'
	@echo 'Local Folder uninstall'
	@echo '    make remove'
	@echo 'System Install'
	@echo '    sudo make install'
	@echo 'System Uninstall'
	@echo '    sudo make uninstall'
	
