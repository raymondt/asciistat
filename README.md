# asciistat 

This program has been developed to perform a character frequency analysis on a text file, reading from  STDIN, and outputs to STDOUT.

This program can be installed by downloading and extracting the source files. Opening a terminal in the new directory and typing "sudo make install" or "make" to see the installation options.
