/*
 *  FILE: 		asciistat.c
 *  CREATED: 		07 August 2019
 *  AUTHOR: 		Raymond Thomson
 *  CONTACT: 		raymond.thomson76@gmail.com
 *  COPYRIGHT:		Free Use
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#define firstc 33
#define lastc 126

int main(int argc,char *argv[]) {

	int pc = lastc - firstc +1;
	int ascii[pc];
	char buf[BUFSIZ];
	int tc;
	int tot = 0;

	for (int i = 0; i < pc; i++) ascii[i] = 0;

	while ((tc = read(0, buf, BUFSIZ)) > 0) {
		for (int i = 0;i < tc;i++) {
			if (buf[i] >= firstc && buf[i] <= lastc) {
				ascii[buf[i]-firstc]++;
				tot++;
			}
		}
	}

	if (argc == 2 && strstr(argv[1],"-v") != NULL) {
		for (int i = 0; i < pc; i++) printf("%i\n",ascii[i]);
 	} else if (argc == 2 && strstr(argv[1],"-n") != NULL) {
		for (int i = 0; i < pc; i++) printf("%09i=%c\n",ascii[i],i+firstc);
	} else {	
		printf("total=%i\n",tot);
		for (int i = 0; i < pc; i++) printf("%c=%i\n",i+firstc,ascii[i]);
	}
	return 0;
}
